自作非同期ランタイムとその上で動く簡易なHTTPサーバー

関連
* [HTTP Serverを実行するための簡素な非同期ランタイムを作った](https://zenn.dev/toru3/articles/9bbb620e0637cd)

参考
* [Rustのasync/awaitを使ったecho serverの実装](https://mmi.hatenablog.com/entry/2019/09/29/203156)
* [RustのFutureとそのRunnerを作ってみた](https://keens.github.io/blog/2019/07/07/rustnofuturetosonorunnerwotsukuttemita/)
