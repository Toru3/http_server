use core::{fmt, panic};
use std::{error, io};

#[derive(thiserror::Error, Debug)]
pub enum Error {
    #[error(transparent)]
    Io(#[from] io::Error),
    #[error(transparent)]
    Fmt(#[from] fmt::Error),
    #[error(transparent)]
    HttpHeader(#[from] httparse::Error),
}
impl From<nix::errno::Errno> for Error {
    fn from(e: nix::errno::Errno) -> Self {
        Self::Io(e.into())
    }
}
#[derive(Debug)]
pub struct LocatedError<E> {
    pub inner: E,
    pub location: &'static panic::Location<'static>,
}
impl<E: error::Error + 'static> fmt::Display for LocatedError<E> {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "{}: {}", self.location, self.inner)
    }
}
impl<E: error::Error + 'static> error::Error for LocatedError<E> {
    fn source(&self) -> Option<&(dyn error::Error + 'static)> {
        Some(&self.inner)
    }
}
impl<E: Into<Error>> From<E> for LocatedError<Error> {
    #[track_caller]
    fn from(err: E) -> Self {
        Self {
            inner: err.into(),
            location: panic::Location::caller(),
        }
    }
}
pub type Result<T> = std::result::Result<T, LocatedError<Error>>;
