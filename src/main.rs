use error::Result;
use nix::sys::signal as nix_signal;
use std::{
    fmt::Write as _,
    fs,
    io::{self, Read as _, Seek as _},
};
mod error;
mod net;
mod signal;
mod task;

async fn get_root(stream: &mut net::TcpStream, buf: &mut [u8], is_head: bool) -> Result<()> {
    let size = fs::metadata("index.html")?.len();
    let mut s = String::with_capacity(256);
    writeln!(&mut s, "HTTP/1.1 200 OK\r")?;
    writeln!(&mut s, "Content-Type: text/html\r")?;
    writeln!(&mut s, "Content-Length: {size}\r")?;
    writeln!(&mut s, "\r")?;
    stream.write_all(s.as_bytes()).await?;
    if is_head {
        return Ok(());
    }
    let mut pos = 0;
    let mut file = fs::File::open("index.html")?;
    while pos < size {
        let n = file.read(buf)?;
        let m = stream.write(&buf[..n]).await?;
        if m < n {
            let d = (n - m) as i64;
            dbg!(d);
            file.seek(io::SeekFrom::Current(-d))?;
        }
        pos += m as u64;
    }
    Ok(())
}

async fn not_found(stream: &mut net::TcpStream) -> Result<()> {
    let mut s = String::with_capacity(256);
    writeln!(&mut s, "HTTP/1.1 404 Not Found\r")?;
    writeln!(&mut s, "Content-Length: 0\r")?;
    writeln!(&mut s, "\r")?;
    stream.write_all(s.as_bytes()).await?;
    Ok(())
}

async fn bad_request(stream: &mut net::TcpStream, e: httparse::Error) -> Result<()> {
    let e = e.to_string();
    let mut s = String::with_capacity(256);
    writeln!(&mut s, "HTTP/1.1 400 Bad Request\r")?;
    writeln!(&mut s, "Content-Length: {}\r", e.len() + 2)?;
    writeln!(&mut s, "\r")?;
    writeln!(&mut s, "{}\r", e)?;
    stream.write_all(s.as_bytes()).await?;
    Ok(())
}

async fn run(mut stream: net::TcpStream, addr: std::net::SocketAddr) -> Result<()> {
    let mut buf = [0; 4096];
    let mut v = Vec::new();
    loop {
        v.clear();
        while !v.windows(4).any(|e| e == [b'\r', b'\n', b'\r', b'\n']) {
            let n = stream.read(&mut buf).await?;
            if n == 0 {
                return Ok(());
            }
            v.extend_from_slice(&buf[..n]);
        }
        let mut headers = [httparse::EMPTY_HEADER; 16];
        let mut req = httparse::Request::new(&mut headers);
        match req.parse(&v) {
            Ok(_) => {
                if req.method == Some("GET") && req.path == Some("/") {
                    println!("GET / from {addr}");
                    get_root(&mut stream, &mut buf, false).await?;
                } else if req.method == Some("HEAD") && req.path == Some("/") {
                    println!("HEAD / from {addr}");
                    get_root(&mut stream, &mut buf, true).await?;
                } else {
                    not_found(&mut stream).await?;
                }
            }
            Err(e) => bad_request(&mut stream, e).await?,
        }
    }
}

fn main() -> Result<()> {
    let sigset = {
        let mut ss = nix_signal::SigSet::empty();
        ss.add(nix_signal::Signal::SIGHUP);
        ss.add(nix_signal::Signal::SIGTERM);
        ss
    };
    let mut signal_fd = signal::SignalFd::new(&sigset)?;
    let listener = net::TcpListener::bind("0.0.0.0:8001")?;
    let mut runtime = task::Runtime::new(16384)?;
    let spawner = runtime.spawner();
    spawner.spawn(async move {
        loop {
            let signal = signal_fd.read().await?;
            let signal = nix_signal::Signal::try_from(signal.ssi_signo as i32).unwrap();
            match signal {
                nix_signal::Signal::SIGHUP => {
                    println!("SIGHUP");
                }
                nix_signal::Signal::SIGTERM => std::process::exit(0),
                _ => unreachable!(),
            }
        }
    });
    let spawner2 = spawner.clone();
    spawner.spawn(async move {
        loop {
            let (s, a) = listener.accept().await?;
            spawner2.spawn(async move {
                if let Err(e) = run(s, a).await {
                    eprintln!("{e}");
                }
                Ok(())
            });
        }
    });
    runtime.run()
}
