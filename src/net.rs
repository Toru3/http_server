use crate::task::{epoll_add, epoll_delete, ok};
use crate::Result;
use core::{
    future::Future,
    pin::Pin,
    task::{self, Poll},
};
use nix::sys::epoll;
use std::{io, net, os::fd::AsFd};

fn is_would_block(e: &io::Error) -> bool {
    e.kind() == io::ErrorKind::WouldBlock
}
pub struct ReadFuture<'a, R: io::Read> {
    inner: &'a mut R,
    buf: &'a mut [u8],
}
impl<'a, R: io::Read + AsFd> Future for ReadFuture<'a, R> {
    type Output = Result<usize>;
    fn poll(self: Pin<&mut Self>, c: &mut task::Context) -> Poll<Self::Output> {
        let Self { inner, buf } = self.get_mut();
        match inner.read(buf) {
            Err(e) if is_would_block(&e) => ok(epoll_add(
                inner,
                epoll::EpollFlags::EPOLLIN,
                c.waker().clone(),
            )),
            r => Poll::Ready(r.map_err(|e| e.into())),
        }
    }
}
pub struct WriteFuture<'a, W: io::Write> {
    inner: &'a mut W,
    buf: &'a [u8],
}
impl<'a, W: io::Write + AsFd> Future for WriteFuture<'a, W> {
    type Output = Result<usize>;
    fn poll(self: Pin<&mut Self>, c: &mut task::Context) -> Poll<Self::Output> {
        let Self { inner, buf } = self.get_mut();
        match inner.write(buf) {
            Err(e) if is_would_block(&e) => ok(epoll_add(
                inner,
                epoll::EpollFlags::EPOLLOUT,
                c.waker().clone(),
            )),
            r => Poll::Ready(r.map_err(|e| e.into())),
        }
    }
}
pub struct TcpStream(net::TcpStream);

impl Drop for TcpStream {
    fn drop(&mut self) {
        epoll_delete(&self.0).unwrap();
    }
}
impl TcpStream {
    pub fn read<'a>(&'a mut self, buf: &'a mut [u8]) -> ReadFuture<'a, net::TcpStream> {
        ReadFuture {
            inner: &mut self.0,
            buf,
        }
    }
    pub fn write<'a>(&'a mut self, buf: &'a [u8]) -> WriteFuture<'a, net::TcpStream> {
        WriteFuture {
            inner: &mut self.0,
            buf,
        }
    }
    pub async fn write_all(&mut self, buf: &[u8]) -> Result<()> {
        let n = buf.len();
        let mut pos = 0;
        while pos < n {
            pos += self.write(buf).await?;
        }
        Ok(())
    }
}

pub struct AcceptFuture<'a>(&'a net::TcpListener);

impl<'a> Future for AcceptFuture<'a> {
    type Output = Result<(TcpStream, net::SocketAddr)>;
    fn poll(self: Pin<&mut Self>, c: &mut task::Context) -> Poll<Self::Output> {
        match self.0.accept() {
            Err(e) if is_would_block(&e) => ok(epoll_add(
                self.0,
                epoll::EpollFlags::EPOLLIN,
                c.waker().clone(),
            )),
            Err(e) => Poll::Ready(Err(e.into())),
            Ok((s, a)) => {
                s.set_nonblocking(true)?;
                let v = (TcpStream(s), a);
                Poll::Ready(Ok(v))
            }
        }
    }
}
pub struct TcpListener(net::TcpListener);

impl Drop for TcpListener {
    fn drop(&mut self) {
        epoll_delete(&self.0).unwrap();
    }
}
impl TcpListener {
    pub fn bind<A: net::ToSocketAddrs>(addr: A) -> Result<Self> {
        let inner = net::TcpListener::bind(addr)?;
        inner.set_nonblocking(true)?;
        Ok(Self(inner))
    }
    pub fn accept(&self) -> AcceptFuture {
        AcceptFuture(&self.0)
    }
}
