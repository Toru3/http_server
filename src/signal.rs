use crate::task::{epoll_add, epoll_delete, ok};
use crate::Result;
use core::{
    future::Future,
    pin::Pin,
    task::{self, Poll},
};
use nix::sys::{epoll, signal, signalfd};

pub struct SignalFdFuture<'a>(&'a mut signalfd::SignalFd);

impl<'a> Future for SignalFdFuture<'a> {
    type Output = Result<signalfd::siginfo>;
    fn poll(mut self: Pin<&mut Self>, c: &mut task::Context) -> Poll<Self::Output> {
        match self.0.read_signal() {
            Ok(None) => ok(epoll_add(
                &self.0,
                epoll::EpollFlags::EPOLLIN,
                c.waker().clone(),
            )),
            Ok(Some(s)) => Poll::Ready(Ok(s)),
            Err(e) => Poll::Ready(Err(e.into())),
        }
    }
}

pub struct SignalFd(signalfd::SignalFd);

impl Drop for SignalFd {
    fn drop(&mut self) {
        epoll_delete(&self.0).unwrap();
    }
}
impl SignalFd {
    pub fn new(ss: &signal::SigSet) -> Result<Self> {
        // シグナルハンドラを起動させないようにブロックしておく
        signal::sigprocmask(signal::SigmaskHow::SIG_BLOCK, Some(ss), None)?;
        let inner = signalfd::SignalFd::with_flags(ss, signalfd::SfdFlags::SFD_NONBLOCK)?;
        Ok(Self(inner))
    }
    pub fn read(&mut self) -> SignalFdFuture {
        SignalFdFuture(&mut self.0)
    }
}
