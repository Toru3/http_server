use crate::Result;
use core::{future::Future, pin::Pin, task};
use nix::sys::epoll;
use std::{
    collections,
    os::fd::{self, AsRawFd},
    sync::{atomic, mpsc, Arc, Mutex},
};

pub fn ok<T>(r: Result<()>) -> task::Poll<Result<T>> {
    if let Err(e) = r {
        task::Poll::Ready(Err(e))
    } else {
        task::Poll::Pending
    }
}
struct Reactor {
    epoll: Arc<epoll::Epoll>,
    epoll_events: collections::HashMap<u64, epoll::EpollFlags>,
    wakers: collections::HashMap<u64, task::Waker>,
}
impl Reactor {
    fn new() -> Result<Self> {
        Ok(Self {
            epoll: Arc::new(epoll::Epoll::new(epoll::EpollCreateFlags::empty())?),
            epoll_events: collections::HashMap::new(),
            wakers: collections::HashMap::new(),
        })
    }
}
static REACTOR: Mutex<Option<Reactor>> = Mutex::new(None);
fn mut_or_init(reactor: &mut Option<Reactor>) -> Result<&mut Reactor> {
    if reactor.is_none() {
        *reactor = Some(Reactor::new()?);
    }
    Ok(reactor.as_mut().unwrap())
}
pub fn epoll_add<F: fd::AsFd>(fd: F, events: epoll::EpollFlags, waker: task::Waker) -> Result<()> {
    let mut reactor = REACTOR.lock().unwrap();
    let reactor = mut_or_init(&mut reactor)?;
    let raw_fd = fd.as_fd().as_raw_fd() as u64;
    if let Some(e) = reactor.epoll_events.get(&raw_fd) {
        if *e != events {
            reactor
                .epoll
                .modify(fd, &mut epoll::EpollEvent::new(events, raw_fd))?;
            reactor.epoll_events.insert(raw_fd, events);
        }
    } else {
        reactor
            .epoll
            .add(fd, epoll::EpollEvent::new(events, raw_fd))?;
        reactor.epoll_events.insert(raw_fd, events);
    }
    reactor.wakers.insert(raw_fd, waker);
    Ok(())
}
pub fn epoll_delete<F: fd::AsFd>(fd: F) -> Result<()> {
    let mut reactor = REACTOR.lock().unwrap();
    let reactor = mut_or_init(&mut reactor)?;
    let raw_fd = fd.as_fd().as_raw_fd() as u64;
    if reactor.epoll_events.get(&raw_fd).is_some() {
        reactor.epoll_events.remove(&raw_fd);
        // close時には勝手にepollの登録が解除されるためdeleteを呼ぶ必要はない
        //reactor.epoll.delete(fd)?;
    }
    Ok(())
}
pub fn epoll_wait(events: &mut [epoll::EpollEvent]) -> Result<usize> {
    let mut reactor = REACTOR.lock().unwrap();
    let reactor = mut_or_init(&mut reactor)?;
    let n = reactor.epoll.wait(events, -1)?;
    for e in &events[..n] {
        let raw_fd = e.data();
        // waker が登録されていればそれを使ってタスクをキューに入れる
        if reactor.wakers.get(&raw_fd).is_some() {
            reactor.wakers.remove(&raw_fd).unwrap().wake_by_ref();
        }
    }
    Ok(n)
}

type BoxFuture<T> = Pin<Box<dyn Future<Output = T> + Send>>;
type ResultFuture = Arc<Mutex<BoxFuture<Result<()>>>>;

// タスクキューに準備が出来たタスクを積むためにタスク自身とsenderがメンバに必要
pub struct WakerInner {
    task: ResultFuture,
    task_sender: mpsc::SyncSender<ResultFuture>,
}
impl WakerInner {
    fn wake_ref(&self) {
        self.task_sender.send(Arc::clone(&self.task)).unwrap();
    }
}
pub struct Waker(Arc<WakerInner>);

impl Waker {
    unsafe fn into_raw_waker(self) -> task::RawWaker {
        let ptr = Arc::into_raw(self.0) as *const ();
        static WAKER_VTABLE: task::RawWakerVTable = task::RawWakerVTable::new(
            Waker::unsafe_clone,
            Waker::unsafe_wake,
            Waker::unsafe_wake_by_ref,
            Waker::unsafe_drop,
        );
        task::RawWaker::new(ptr, &WAKER_VTABLE)
    }

    pub fn waker(inner: Arc<WakerInner>) -> task::Waker {
        let b = Box::new(Self(inner));
        unsafe { task::Waker::from_raw(b.into_raw_waker()) }
    }
    fn wake_ref(&self) {
        self.0.wake_ref()
    }

    unsafe fn unsafe_clone(this: *const ()) -> task::RawWaker {
        let ptr = this as *const WakerInner;
        let arc = Arc::from_raw(ptr);
        let ret = Self(Arc::clone(&arc)).into_raw_waker();
        std::mem::forget(arc);
        ret
    }
    unsafe fn unsafe_wake(this: *const ()) {
        let ptr = this as *const WakerInner;
        Self(Arc::from_raw(ptr)).wake_ref()
    }
    unsafe fn unsafe_wake_by_ref(this: *const ()) {
        let ptr = this as *const WakerInner;
        let arc = Arc::from_raw(ptr);
        let ret = Self(Arc::clone(&arc));
        std::mem::forget(arc);
        ret.wake_ref()
    }
    unsafe fn unsafe_drop(this: *const ()) {
        let ptr = this as *const WakerInner;
        Self(Arc::from_raw(ptr));
    }
}

#[derive(Clone)]
pub struct Spawner {
    /// Runtime とカウンターを共有する必要がある
    unfinished: Arc<atomic::AtomicU64>,
    task_sender: mpsc::SyncSender<ResultFuture>,
}
impl Spawner {
    pub fn spawn(&self, f: impl Future<Output = Result<()>> + Send + 'static) {
        self.unfinished.fetch_add(1, atomic::Ordering::Relaxed);
        self.task_sender
            .send(Arc::new(Mutex::new(Box::pin(f))))
            .unwrap();
    }
}
pub struct Runtime {
    /// Spawner とカウンターを共有する必要がある
    unfinished: Arc<atomic::AtomicU64>,
    task_sender: mpsc::SyncSender<ResultFuture>,
    /// RuntimeをClone出来ない型にしておくことでReceiverが一つだけになる
    task_receiver: mpsc::Receiver<ResultFuture>,
    events: Box<[epoll::EpollEvent]>,
}
impl Runtime {
    pub fn new(bound: usize) -> Result<Self> {
        let (task_sender, task_receiver) = mpsc::sync_channel(bound);
        let unfinished = Arc::new(atomic::AtomicU64::new(0));
        Ok(Self {
            unfinished,
            task_sender,
            task_receiver,
            events: vec![epoll::EpollEvent::empty(); bound].into_boxed_slice(),
        })
    }
    pub fn spawner(&self) -> Spawner {
        Spawner {
            unfinished: Arc::clone(&self.unfinished),
            task_sender: self.task_sender.clone(),
        }
    }
    pub fn run(&mut self) -> Result<()> {
        while self.unfinished.load(atomic::Ordering::Relaxed) > 0 {
            let task = match self.task_receiver.try_recv() {
                Err(mpsc::TryRecvError::Empty) => {
                    epoll_wait(&mut self.events)?;
                    continue;
                }
                Err(mpsc::TryRecvError::Disconnected) => unreachable!(),
                Ok(v) => v,
            };
            //dbg!(Arc::as_ptr(&task));
            let waker = Waker::waker(Arc::new(WakerInner {
                task: Arc::clone(&task),
                task_sender: self.task_sender.clone(),
            }));
            let mut c = task::Context::from_waker(&waker);
            let r = task.lock().unwrap().as_mut().poll(&mut c);
            match r {
                task::Poll::Ready(Ok(())) => {
                    assert!(self.unfinished.fetch_sub(1, atomic::Ordering::Relaxed) > 0);
                }
                task::Poll::Ready(Err(e)) => return Err(e),
                task::Poll::Pending => {}
            }
        }
        Ok(())
    }
}
